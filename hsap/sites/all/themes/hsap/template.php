<?php

function hsap_preprocess_page(&$variables)
{
    $variables['hsap_class'] = "left one-third";
    if( empty($variables['page']['left']) && empty($variables['page']['right']) ){
        $variables['hsap_class'] = "two-thirds auto";
    }
    elseif (!empty($variables['page']['left']) && empty($variables['page']['right'])) {
        $variables['hsap_class'] = "left one-half";
    }
}

?>
