<div id="header" class="fullWidth">
    <div id="headerInner" class="pageWidth auto">
        <div id="title" class="left">Homeschool Assistance Program</div>
        <div class="right">
            <?php 
                print theme('links',array('links'=>$main_menu));
            ?>
        </div>
    </div>
</div>

<div class="pageWidth auto">

    <?php if ($messages): ?>
        <div id="messages">
          <?php print $messages; ?>
        </div>
    <?php endif; ?>

    <?php if ($tabs): ?>
        <div class="tabs">
            <?php print render($tabs); ?>
        </div>
    <?php endif; ?>

    <!-- left region -->
    <div class="<?php print $variables['hsap_class']; ?>">
        <?php
            print render($page['left']);
        ?>
    </div>

    <!-- content region -->
    <div class="<?php print $variables['hsap_class']; ?>">
        
        <?php
            print render($page['content']);
        ?>

    </div>
    
    <!-- right region -->
    <div class="left one-third">
        <?php
            print render($page['right']);
        ?>
    </div>

</div>

<div class="footer fullWidth centered">
    &copy; Lee Anderson 2015
</div>
